use std::net;
use std::io::prelude::*;

pub fn connect(address: &'static str) -> net::TcpStream{
    loop {
        let stream = net::TcpStream::connect(address);
        match stream {
            Ok(stream) => {
                println!("connected!");
                return stream;
            },
            Err(_e) => ()
        }
    }
}
