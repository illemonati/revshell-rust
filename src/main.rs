

const ADDRESS: &'static str = "192.168.0.3:9876";
mod command_shell;
mod utils;
mod simple_shell;

fn main() {
//    command_shell::run(ADDRESS);
    simple_shell::run(ADDRESS);
}

